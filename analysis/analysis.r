library(xtable)

bkv <- read.csv(file = "Material/Best-known Values", header = FALSE,  sep = " ", row.names = NULL,  stringsAsFactors = FALSE)$V2
bestKnownVal_50 <- rep(bkv[1:10], each=5)
bestKnownVal_100 <- rep(bkv[11:20], each=5)



val_SA_50 <- read.csv("./dat/SA_bench50.csv")
valMat_SA_50 <- data.matrix(val_SA_50[c("rep_1","rep_2","rep_3","rep_4","rep_5")])
devMat_SA_50 <- 100*(valMat_SA_50 - bestKnownVal_50)/bestKnownVal_50
meanDev_SA_50 <- rowMeans(devMat_SA_50)

val_SA_100 <- read.csv("./dat/SA_bench100.csv")
valMat_SA_100 <- data.matrix(val_SA_100[c("rep_1","rep_2","rep_3","rep_4","rep_5")])
devMat_SA_100 <- 100*(valMat_SA_100 - bestKnownVal_100)/bestKnownVal_100
meanDev_SA_100 <- rowMeans(devMat_SA_100)


val_RG_50 <- read.csv("./dat/RG_bench50.csv")
valMat_RG_50 <- data.matrix(val_RG_50[c("rep_1","rep_2","rep_3","rep_4","rep_5")])
devMat_RG_50 <- 100*(valMat_RG_50 - bestKnownVal_50)/bestKnownVal_50
meanDev_RG_50 <- rowMeans(devMat_RG_50)

val_RG_100 <- read.csv("./dat/RG_bench100.csv")
valMat_RG_100 <- data.matrix(val_RG_100[c("rep_1","rep_2","rep_3","rep_4","rep_5")])
devMat_RG_100 <- 100*(valMat_RG_100 - bestKnownVal_100)/bestKnownVal_100
meanDev_RG_100 <- rowMeans(devMat_RG_100)


png("analysis/cor_50.png", width=600, height=600)

plot(meanDev_RG_50, meanDev_SA_50, pch=19, col="blue", main="Correlation plot of the average relative percentage deviation\nbetween the two metaheuristics for instance of size 50", xlab="ARPD for Reactive GRASP", ylab="ARPD for Simulated Annealing")
abline(lm(meanDev_SA_50 ~ meanDev_RG_50), col = "red", lwd = 3)

dev.off()

png("analysis/cor_100.png", width=600, height=600)

plot(meanDev_RG_100, meanDev_SA_100, pch=19, col="blue", main="Correlation plot of the average relative percentage deviation\nbetween the two metaheuristics for instance of size 100", xlab="ARPD for Reactive GRASP", ylab="ARPD for Simulated Annealing")
abline(lm(meanDev_SA_100 ~ meanDev_RG_100), col = "red", lwd = 3)

dev.off()



wilcox50 <- wilcox.test(devMat_SA_50, devMat_RG_50, paired=T)$p.value
wilcox100 <- wilcox.test(devMat_RG_100, devMat_SA_100, paired=T)$p.value


