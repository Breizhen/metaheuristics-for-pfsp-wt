# Read Me - Heuristic Optimization - PFSP 

## Contents

### **`src` folder**

Contains the C++ files (classes and functions definition) as defined in the report file (`Report.pdf`).

### **`instances` folder**

Contains all the instances used for the analyses of the programms.

### **`dat` folder**
Contains the CSV files with the weighted tardinnes and the computation time of the different algorithms on the different insatnces.

### **`analysis` folder**

Contains the R file made to analyze the data in the CSV files presented above.


***

## Execution

To compile and execute the main function of the program, the user should open a terminal in the `heuristic-optimization-pfsp` folder and use the following command:

```bash
make && ./flowshopMH <MetaH> <pathToInstance>
```
with `<MetaH>` = `--SA`  or  `--RG`.

After this command is launched, depending on the chosen metaheuristic, you will be asked to enter the different parameters of this metaheuristic.

***

### Output

Exemple of output:


**With the parameters entered, the returned solution is:** 122717
