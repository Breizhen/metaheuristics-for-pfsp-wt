//#ifndef FLOWSHOP_H_
//#define FLOWSHOP_H_

#include <vector>
#include "pfspinstance.h"

long int getWT(vector<int> sol, vector<vector<int>> endTimes, PfspInstance inst);
vector<int> sortWSPT(PfspInstance instance);
pair<long int, vector<vector<int> > > partialWCT(vector<int> sortedJobs, PfspInstance inst);
pair<vector<int>, vector<vector<int> > > RZHeuristic(PfspInstance inst);

vector<vector<int>> updateEndTimes(PfspInstance inst, vector<vector<int> > & endTimes, vector<int> & newSol, int firstChange);
map<pair<int, int>, pair<long int, vector<vector<int> > > > exchangeNBH(PfspInstance inst, vector<vector<int> > endTimes, vector<int> currentSol);

pair<int, int> bestImprov_EI(map<pair<int, int>, pair<long int, vector<vector<int>>>> nbh, long int currentVal);