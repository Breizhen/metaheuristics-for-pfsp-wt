/***************************************************************************
 *   Copyright (C) 2012 by Jérémie Dubois-Lacoste   *
 *   jeremie.dl@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <numeric>
#include <iostream>
#include <vector>
#include <string>
#include <bits/stdc++.h>
#include <fstream>
#include <filesystem>
#include <chrono>

#include "pfspinstance.h"
#include "flowshop.h"

using namespace std;
using directory_iterator = std::filesystem::directory_iterator;




long int getWT(vector<int> sol, vector<vector<int>> endTimes, PfspInstance inst){
    long int wghtdTard = 0L;
    int n = inst.getNbJob();
    int m = inst.getNbMac();

    for (int i = 1; i <= n; i++){
        wghtdTard += inst.getPriority(sol[i])*(max(long(0), endTimes[i-1][m-1] - inst.getDueDate(sol[i])));
    }

    return wghtdTard;
}


/*I. INITIAL SOLUTION*/
/*  1. Random Uniform Permutation --- OK */

int generateRndPosition(int min, int max){
  return ( rand() % max + min );
}


/* Fill the solution with numbers between 1 and nbJobs, shuffled */
void randomPermutation(int nbJobs, vector< int > & sol){
    vector<bool> alreadyTaken(nbJobs+1, false); // nbJobs elements with value false
    vector<int> choosenNumber(nbJobs+1, 0);

    int nbj;
    int rnd, i, j, nbFalse;

    nbj = 0;
    for (i = nbJobs; i >= 1; --i){
        rnd = generateRndPosition(1, i);
        nbFalse = 0;

        /* find the rndth cell with value = false : */
        for (j = 1; nbFalse < rnd; ++j)
            if ( ! alreadyTaken[j] )
                ++nbFalse;
        --j;

        sol[j] = i;

        ++nbj;
        choosenNumber[nbj] = j;

        alreadyTaken[j] = true;
    }
}

/*  2. Simplified RZ Heuristic --- OK!  */

//Get jobs sorted by ascending weighted sum of processing time

vector<int> sortWSPT(PfspInstance instance){
    vector<float> wspt;
    wspt.resize(instance.getNbJob()+1);
    wspt[0] = INT_MAX;
    for (int i = 1; i <= instance.getNbJob(); i++){
        float acc = 0.;
        for (int j = 1; j <= instance.getNbMac(); j++){
            acc += (float)instance.getTime(i,j);
        }
        wspt[i] = acc / instance.getPriority(i);
    }
    
    vector<int> srtWSPT;
    srtWSPT.resize(instance.getNbJob());
    for (int i = 0; i < instance.getNbJob(); i++){
        srtWSPT[i] = distance(wspt.begin(), min_element(wspt.begin(), wspt.end()));
        wspt[srtWSPT[i]] = INT_MAX;
    }
    
    return srtWSPT;
}



pair<long int, vector<vector<int> > > partialWCT(vector<int> sortedJobs, PfspInstance inst){
    
    /*
        endTimes is defined as (with aij the end time of the job i on machine j, jobs sorted by order in the solution):
        
                mac_1  mac_2 ... mac_m
        job_1  (a11,   a12,  ..., a1m )
        ...    (...,   ...,  ..., ... )
        job_n  (an1,   an2,  ..., anm )
    */

    int nbJobs = sortedJobs.size();
    int m = inst.getNbMac();
    vector<vector<int> > endTimes;
    endTimes.resize(nbJobs);
    for (int i = 0; i < nbJobs; i++){
        endTimes[i].resize(m);
    }

    int cpt = 0;
    for (int i = 0; i < m; i++){
        cpt += inst.getTime(sortedJobs[0], i+1);
        endTimes[0][i] = cpt;
    }

    for (int i = 1; i < nbJobs; i++){
        cpt = endTimes[i-1][0] + inst.getTime(sortedJobs[i],1);
        endTimes[i][0] = cpt;
        for (int j = 1; j < inst.getNbMac(); j++){
            cpt += max(0, endTimes[i-1][j] - endTimes[i][j-1]) + inst.getTime(sortedJobs[i], j+1);
            endTimes[i][j] = cpt;
        }
    }

    long int pWCT = 0;
    for (int i = 0; i < nbJobs; i++){

        pWCT += inst.getPriority(sortedJobs[i]) * endTimes[i][inst.getNbMac()-1];
    }

    return make_pair(pWCT, endTimes);   
}

pair<vector<int>, vector<vector<int> > > RZHeuristic(PfspInstance inst){
    int n = inst.getNbJob();
    vector<int> preSol = sortWSPT(inst);
    vector<int> finSol;
    finSol.insert(finSol.begin(), preSol[0]);
    
    //Iterating on elts of preSol (except for first elt cause already added)
    for (int i = 1; i < n; i++){
        vector<int> partSol = finSol;
        int *times;
        times = new int [i+1];
        for (int j = 0; j <= i; j++){
            partSol.insert(partSol.begin() + j, preSol[i]);
            times[j] = partialWCT(partSol, inst).first;
            partSol.erase(partSol.begin() + j);
        }
        finSol.insert(finSol.begin() + (min_element(times, times+i) - times), preSol[i]);
        delete[] times;
    }

    vector<vector<int> > endTimes = partialWCT(finSol,inst).second;

    finSol.insert(finSol.begin(), 0); //We ignore first element (index 0)
    return make_pair(finSol, endTimes);
}




/* II. NEIGHBORHOOD */
/*  0. Speed-Up */

vector<vector<int>> updateEndTimes(PfspInstance inst, vector<vector<int> > & endTimes, vector<int> & newSol, int firstChange){

    int n = inst.getNbJob();
    int m = inst.getNbMac();
    int cpt;

    if (firstChange == 1){
        newSol.erase(newSol.begin());
        endTimes = partialWCT(newSol, inst).second;
        newSol.insert(newSol.begin(),0);
    }else{
        for (int i = firstChange; i <= n; i++){
            cpt = endTimes[i-2][0] + inst.getTime(newSol[i],1);
            endTimes[i-1][0] = cpt;
            for (int j = 1; j <= m; j++){
                cpt += max(endTimes[i-2][j], endTimes[i-1][j-1]) + inst.getTime(newSol[i], j);
                endTimes[i-1][j-1] = cpt;
            }
        }
    }

    return endTimes;
}

/*  1. Transpose Neighborhood */

vector<pair<long int, vector<vector<int> > > > transposeNBH(PfspInstance inst, vector<vector<int> > endTimes, vector<int> currentSol){
    int n = currentSol.size();
    vector< pair <long int, vector<vector<int> > > > nbh;
    nbh.resize(n-1);
    nbh[0] = make_pair(LONG_MAX, vector<vector<int>>());
    vector<int> modSol;
    vector<vector<int> > modEndTimes;
    int temp;

    for (int i = 1; i < n-1; i++){
        modSol = currentSol;
        modEndTimes = endTimes;
        temp = modSol[i];
        modSol[i] = modSol[i+1];
        modSol[i+1] = temp;
        nbh[i] = make_pair(getWT(modSol, endTimes, inst), updateEndTimes(inst, modEndTimes, modSol, i));
    }
    
    return nbh;
}

/*  2. Exchange Neighborhood */

map<pair<int, int>, pair<long int, vector<vector<int> > > > exchangeNBH(PfspInstance inst, vector<vector<int> > endTimes, vector<int> currentSol){
    int n = inst.getNbJob();
    map<pair<int, int>, pair<long int, vector<vector<int> > > > nbh;
    vector<int> modSol;
    vector<vector<int> > modEndTimes;
    int temp;
    

    for (int i = 1; i < n; i++){
        for (int j = i+1; j <= n; j++){
            modSol = currentSol;
            modEndTimes = endTimes;
            temp = modSol[i];
            modSol[i] = modSol[j];
            modSol[j] = temp;

            //DEBUG
            long int x = getWT(modSol, endTimes, inst);
            if (x <= 0) cout << "\nx = " << x << endl;
            //DEBUG

            nbh[make_pair(i,j)] = make_pair(getWT(modSol, endTimes, inst), updateEndTimes(inst, modEndTimes, modSol, i));
        }
    }

    /*
    cout << "\n[";
    for (auto it = nbh.begin(); it != nbh.end(); it++){
        if ((it->second).first <= 0){
            cout << (it->second).first << ",  ";
        }
    }
    cout << "]\n" << endl;
    */

    return nbh;
}

/*  3. Insert Neighborhood */

map<pair<int, int>, pair<long int, vector<vector<int> > > > insertNBH(PfspInstance inst, vector<vector<int> > endTimes, vector<int> currentSol){
    int n = inst.getNbJob();
    map<pair<int, int>, pair<long int, vector<vector<int> > > > nbh;
    vector<int> modSol;
    vector<vector<int> > modEndTimes;
    int temp;

    for (int i = 1; i < n; i++){
        for (int j = 1; j <= n; j++){
            if (j != i){
                modSol = currentSol;
                modEndTimes = endTimes;
                temp = modSol[i];
                modSol.erase(modSol.begin()+i);
                modSol.insert(modSol.begin()+j, temp);
                nbh[make_pair(i,j)] = make_pair(getWT(modSol, endTimes, inst), updateEndTimes(inst, modEndTimes, modSol, i));
            }
        }
    }
    
    return nbh;
}


/* III. PIVOTING RULE */
/*  1a. First-Improvement - Transpose*/

int firstImprov_T(vector<pair<long int, vector<vector<int>>>> tNBH, long int currentVal){
    auto isBetter = [&, currentVal](pair<long int, vector<vector<int>>> x){ return x.first < currentVal; };

    if (auto it = find_if(tNBH.begin(), tNBH.end(), isBetter); it != tNBH.end()){
        return it - tNBH.begin();
    }else{
        return -1;
    }

}

/*   1b. First-Improvement - Exchange & Insert*/

pair<int, int> firstImprov_EI(map<pair<int, int>, pair<long int, vector<vector<int>>>> nbh, long int currentVal){

    auto isBetter = [&, currentVal](const pair<pair<int, int>, pair<long int, vector<vector<int>>>> & x) -> bool { return x.second.first < currentVal; };

    if (auto it = find_if(nbh.begin(), nbh.end(), isBetter); it != nbh.end()){
        return it->first;
    }else{
        return make_pair(0,0);
    }
}

/*  2a. Best-Improvement - Transpose*/

int bestImprov_T(vector<pair<long int, vector<vector<int>>>> tNBH, long int currentVal){
    auto isBest = [](pair<long int, vector<vector<int>>> &a, pair<long int, vector<vector<int>>> &b){ return a.first < b.first; };

    if (auto it = min_element(tNBH.begin(), tNBH.end(), isBest); (it != tNBH.end()) && ((*it).first < currentVal)){
        return it - tNBH.begin();
    }else{
        return -1;
    }
}

/*   2b. Best-Improvement - Exchange & Insert*/

pair<int, int> bestImprov_EI(map<pair<int, int>, pair<long int, vector<vector<int>>>> nbh, long int currentVal){

    auto isBest = [](const pair<pair<int, int>, pair<long int, vector<vector<int>>>> & a, const pair<pair<int, int>, pair<long int, vector<vector<int>>>> & b) -> bool { return a.second.first < b.second.first; };

    if (auto it = min_element(nbh.begin(), nbh.end(), isBest); (it != nbh.end()) && ((it->second).first < currentVal)){
        return it->first;
    }else{
        return make_pair(0,0);
    }
}


/******* - VND - *******/

/***** - Rand x T.E.I - *****/

pair<long int, double> VND_Rand_TEI(const char *path){
    
    PfspInstance inst;

    if (!inst.readDataFromFile(path)) return make_pair(0L, 0);

    int indNbh = 1;
    int n = inst.getNbJob();

    auto start = chrono::high_resolution_clock::now();

    pair<vector<int>, vector<vector<int>>> solution;
    vector<int> sol(n+1);
    randomPermutation(n, sol);

    sol.erase(sol.begin());
    vector<vector<int>> endTimes = partialWCT(sol, inst).second;
    sol.insert(sol.begin(), 0);

    solution = make_pair(sol, endTimes);

    bool improved = true;
    long int WT = getWT(solution.first, solution.second, inst);
    int temp;

    vector<pair<long int, vector<vector<int>>>> nbhTransp;
    int improvementTransp;
    map<pair<int, int>, pair<long int, vector<vector<int>>>> nbhExIns;
    pair<int, int> improvementExIns;

    while (indNbh < 4){
        switch (indNbh){
        case 1: //Transpose Nbh
            nbhTransp = transposeNBH(inst, solution.second, solution.first);
            improvementTransp = firstImprov_T(nbhTransp, WT);

            if (improvementTransp == -1){
                    improved = false;
            }else{
                improved = true;
                temp = solution.first[improvementTransp];
                solution.first[improvementTransp] = solution.first[improvementTransp+1];
                solution.first[improvementTransp+1] = temp;

                WT = nbhTransp[improvementTransp].first;
            }
        break;

        case 2: //Exchange Nbh
            nbhExIns = exchangeNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }
        
        break;

        case 3: //Insert Nbh
            nbhExIns = insertNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }

        break;
        
        default:
            break;
        }

        indNbh = improved ? 1 : indNbh+1;
    }

    auto end = chrono::high_resolution_clock::now();

    double elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    elapsed *= 1e-9;

    cout << "WT = " << WT << endl;

    return make_pair(WT, elapsed);

}


/***** - Rand x T.I.E - *****/

pair<long int, double> VND_Rand_TIE(const char *path){
    
    PfspInstance inst;

    if (!inst.readDataFromFile(path)) return make_pair(0L, 0);

    int indNbh = 1;
    int n = inst.getNbJob();

    auto start = chrono::high_resolution_clock::now();

    pair<vector<int>, vector<vector<int>>> solution;
    vector<int> sol(n+1);
    randomPermutation(n, sol);

    sol.erase(sol.begin());
    vector<vector<int>> endTimes = partialWCT(sol, inst).second;
    sol.insert(sol.begin(), 0);

    solution = make_pair(sol, endTimes);

    bool improved = true;
    long int WT = getWT(solution.first, solution.second, inst);
    int temp;

    vector<pair<long int, vector<vector<int>>>> nbhTransp;
    int improvementTransp;
    map<pair<int, int>, pair<long int, vector<vector<int>>>> nbhExIns;
    pair<int, int> improvementExIns;

    while (indNbh < 4){
        switch (indNbh){
        case 1: //Transpose Nbh
            nbhTransp = transposeNBH(inst, solution.second, solution.first);
            improvementTransp = firstImprov_T(nbhTransp, WT);

            if (improvementTransp == -1){
                    improved = false;
            }else{
                improved = true;
                temp = solution.first[improvementTransp];
                solution.first[improvementTransp] = solution.first[improvementTransp+1];
                solution.first[improvementTransp+1] = temp;

                WT = nbhTransp[improvementTransp].first;
            }
        break;

        case 2: //Insert Nbh
            nbhExIns = insertNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }
        
        break;

        case 3: //Exchange Nbh
            nbhExIns = exchangeNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }

        break;
        
        default:
            break;
        }

        indNbh = improved ? 1 : indNbh+1;
    }

    auto end = chrono::high_resolution_clock::now();

    double elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    elapsed *= 1e-9;


    cout << "WT = " << WT << endl;

    return make_pair(WT, elapsed);

}



/***** - SRZ x T.E.I - *****/

pair<long int, double> VND_SRZ_TEI(const char *path){
    
    PfspInstance inst;

    if (!inst.readDataFromFile(path)) return make_pair(0L, 0);

    auto start = chrono::high_resolution_clock::now();

    int indNbh = 1;

    pair<vector<int>, vector<vector<int>>> solution = RZHeuristic(inst);
    bool improved = true;
    long int WT = getWT(solution.first, solution.second, inst);
    int temp;

    vector<pair<long int, vector<vector<int>>>> nbhTransp;
    int improvementTransp;
    map<pair<int, int>, pair<long int, vector<vector<int>>>> nbhExIns;
    pair<int, int> improvementExIns;

    while (indNbh < 4){
        switch (indNbh){
        case 1: //Transpose Nbh
            nbhTransp = transposeNBH(inst, solution.second, solution.first);
            improvementTransp = firstImprov_T(nbhTransp, WT);

            if (improvementTransp == -1){
                    improved = false;
            }else{
                improved = true;
                temp = solution.first[improvementTransp];
                solution.first[improvementTransp] = solution.first[improvementTransp+1];
                solution.first[improvementTransp+1] = temp;

                WT = nbhTransp[improvementTransp].first;
            }
        break;

        case 2: //Exchange Nbh
            nbhExIns = exchangeNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }
        
        break;

        case 3: //Insert Nbh
            nbhExIns = insertNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }

        break;
        
        default:
            break;
        }

        indNbh = improved ? 1 : indNbh+1;
    }

    auto end = chrono::high_resolution_clock::now();

    double elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    elapsed *= 1e-9;

    cout << "WT = " << WT << endl;

    return make_pair(WT, elapsed);

}

/***** - SRZ x T.I.E - ******/

pair<long int, double> VND_SRZ_TIE(const char *path){

    PfspInstance inst;

    if (!inst.readDataFromFile(path)) return make_pair(0L, 0);

    auto start = chrono::high_resolution_clock::now();

    int indNbh = 1;

    pair<vector<int>, vector<vector<int>>> solution = RZHeuristic(inst);
    bool improved = true;
    long int WT = getWT(solution.first, solution.second, inst);
    int temp;

    vector<pair<long int, vector<vector<int>>>> nbhTransp;
    int improvementTransp;
    map<pair<int, int>, pair<long int, vector<vector<int>>>> nbhExIns;
    pair<int, int> improvementExIns;

    while (indNbh < 4){
        switch (indNbh){
        case 1: //Transpose Nbh
            nbhTransp = transposeNBH(inst, solution.second, solution.first);
            improvementTransp = firstImprov_T(nbhTransp, WT);

            if (improvementTransp == -1){
                    improved = false;
            }else{
                improved = true;
                temp = solution.first[improvementTransp];
                solution.first[improvementTransp] = solution.first[improvementTransp+1];
                solution.first[improvementTransp+1] = temp;

                WT = nbhTransp[improvementTransp].first;
            }
        break;

        case 2: //Insert Nbh
            nbhExIns = insertNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }
        
        break;

        case 3: //Exchange Nbh
            nbhExIns = exchangeNBH(inst, solution.second, solution.first);
            improvementExIns = firstImprov_EI(nbhExIns, WT);

            if (improvementExIns == make_pair(0,0)){
                improved = false;
            }else{
                temp = solution.first[improvementExIns.first];
                solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                solution.first[improvementExIns.second] = temp;

                WT = nbhExIns[improvementExIns].first;
            }

        break;
        
        default:
            break;
        }

        indNbh = improved ? 1 : indNbh+1;
    }

    auto end = chrono::high_resolution_clock::now();

    double elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    elapsed *= 1e-9;


    cout << "WT = " << WT << endl;

    return make_pair(WT, elapsed);
}


/********************** MAIN TEST **********************/

int main_test(int argc, char *argv[]){
    int i;
    long int WeightedSumCompletionTimes;


    if (argc == 1)
    {
        cout << "Usage: ./flowshopWCT <instance_file>" << endl;
        return 0;
    }

    /* initialize random seed: */
    //srand ( time(NULL) );

    /* Create instance object */
    PfspInstance instance;

    /* Read data from file */
    if (! instance.readDataFromFile(argv[1]) )
        return 1;

    int n = instance.getNbJob();
    int m = instance.getNbMac();

    //DEBUG - PRINTING TABLE OF TIMES
    /*
    for (int i = 1; i <= n; i++){
        for (int j = 1; j <= m; j++){
            cout << instance.getTime(i,j) << " ";
        }
        cout << endl;
    }
    */
    
    
    
    /* Create a vector of int to represent the solution
        WARNING: By convention, we store the jobs starting from index 1,
                thus the size nbJob + 1. */
    //vector< int > solution ( instance.getNbJob()+ 1 );

    /* Fill the vector with a random permutation */
    //randomPermutation(instance.getNbJob(), solution);

    //cout << "Random solution: " ;
    //for (i = 1; i <= instance.getNbJob(); ++i)
    //    cout << solution[i] << " " ;
    //cout << endl;

    /* Compute the WCT of this solution */
    //WeightedSumCompletionTimes = instance.computeWCT(solution);
    //cout << "Total weighted completion time: " << WeightedSumCompletionTimes << endl;

    //cout << "\n" << endl;
    
    cout << "--------------Initial Sol--------------\n" << endl;
    
    
    pair<vector<int>, vector<vector<int> > > solution = RZHeuristic(instance);
    cout << "RZ solution: " ;
    for (i = 1; i <= instance.getNbJob(); ++i)
        cout << solution.first[i] << " " ;
    cout << endl;

    long int WT = getWT(solution.first, solution.second, instance);
    cout << "Weighted Tardiness: " << WT << endl;

    cout << "-----------------------------------------\n\n--------------Neighborhood--------------\n" << endl;


    vector<pair<long int, vector<vector<int> > > > tNBH = transposeNBH(instance, solution.second, solution.first);

    cout << "Transpose Nbh ([item of the first index transposed, Weighted Tardiness]): [";
    for (unsigned int i = 0; i < tNBH.size(); i++){
          //if (i==tNBH.size()-1)
               cout << "(" << i << ", " << tNBH[i].first << ")" << ((i==tNBH.size()-1) ? "]\n" :  ", ");
    }
    cout << endl;

    map<pair<int, int>, pair<long int, vector<vector<int>>>> eNBH = exchangeNBH(instance, solution.second, solution.first);

    map<pair<int, int>, pair<long int, vector<vector<int> > > >::iterator it;
    cout << "Exchange Nbh ([(item exchanged 1, item exchanged 2), Weighted Tardiness]): [";
    for (it = eNBH.begin(); it != eNBH.end(); it++){
        cout << "((" << it->first.first << ", " << it->first.second << "), " << it->second.first << "), "; 
    }
    cout << "]\n" << endl;

    map<pair<int, int>, pair<long int, vector<vector<int>>>> iNBH = insertNBH(instance, solution.second, solution.first);

    //map<pair<int, int>, pair<long int, vector<vector<int> > > >::iterator it;
    cout << "Insert Nbh ([(item exchanged 1, item exchanged 2), Weighted Tardiness]): [";
    for (it = iNBH.begin(); it != iNBH.end(); it++){
        cout << "((" << it->first.first << ", " << it->first.second << "), " << it->second.first << "), "; 
    }
    cout << "]\n" << endl;
    
    cout << "-----------------------------------------\n\n--------------Pivoting Rule--------------\n" << endl;

    /*For test - DELETE AFTER*/
    //WT = 63;

    cout << "First Improve Transpose: " << firstImprov_T(tNBH, WT) << endl;
    cout << "Best Improve Transpose: " << bestImprov_T(tNBH, WT) << endl;

    pair<int, int> fie = firstImprov_EI(eNBH, WT);
    pair<int, int> fii = firstImprov_EI(iNBH, WT);
    cout << "First Improve Exchange - Insert: (" << fie.first << ", " << fie.second << ") - (" << fii.first << ", " << fii.second << ")" << endl;

    pair<int, int> bie = bestImprov_EI(eNBH, WT);
    pair<int, int> bii = bestImprov_EI(iNBH, WT);
    cout << "Best Improve Exchange - Insert: (" << bie.first << ", " << bie.second << ") - (" << bii.first << ", " << bii.second << ")" << endl;
    

    return 0;
}


/*----------------MAIN----------------*/

int real_main(int argc, char *argv[]){
    
    if (argc < 5){
        cout << "ERROR: Usage: ./flowshopWCT <initSol> <neighborhood> <pivotRule> <pathToInstance>" << endl;
        cout << "With:      <initSol> = --randomInit  or  --srz" << endl;
        cout << "      <neighborhood> = --transpose  or  --exchange  or  --insert" << endl;
        cout << "         <pivotRule> = --first  or  --best" << endl;

        return 0;
    }

    PfspInstance inst;

    if (!inst.readDataFromFile(argv[4])) return 0;

    int n = inst.getNbJob();
    int m = inst.getNbMac();

    map<string,int> arg2int = {{"--randomInit", 11},
                               {"--srz", 12},
                               {"--transpose", 21},
                               {"--exchange", 22},
                               {"--insert", 23},
                               {"--first", 31},
                               {"--best", 32}};


    /*--- Initial solution ---*/

    pair<vector<int>, vector<vector<int>>> solution;
    
    auto start = chrono::high_resolution_clock::now();

    switch (arg2int[argv[1]]){
        case 11:{ //--randomInit
            vector<int> sol(n+1);
            randomPermutation(n, sol);

            sol.erase(sol.begin());
            vector<vector<int>> endTimes = partialWCT(sol, inst).second;
            sol.insert(sol.begin(), 0);

            solution = make_pair(sol, endTimes);
            break;
        }
        
        case 12: //--srz
            solution = RZHeuristic(inst);
            break;
        
        default:
            cout << "Invalid argument for initial solution, choose between --randomInit and --srz" << endl;
        return 0;
    }



    bool improved = true;
    int temp;
    long int WT = getWT(solution.first, solution.second, inst);

    vector<pair<long int, vector<vector<int>>>> nbhTransp;
    int improvementTransp;
    map<pair<int, int>, pair<long int, vector<vector<int>>>> nbhExIns;
    pair<int, int> improvementExIns;


    while (improved){

        switch (arg2int[argv[2]]){
            case 21: //--transpose
                nbhTransp = transposeNBH(inst, solution.second, solution.first);                
                switch (arg2int[argv[3]]){
                    case 31: //--first
                        improvementTransp = firstImprov_T(nbhTransp, WT);
                    break;

                    case 32: //--best
                        improvementTransp = bestImprov_T(nbhTransp, WT);
                        
                    break;
                    
                    default:
                        cout << "Invalid argument for initial solution, choose between --first and --best" << endl;
                    return 0;
                }

                if (improvementTransp == -1){
                    improved = false;
                }else{
                    temp = solution.first[improvementTransp];
                    solution.first[improvementTransp] = solution.first[improvementTransp+1];
                    solution.first[improvementTransp+1] = temp;

                    WT = nbhTransp[improvementTransp].first;
                }

            break;
            
            case 22: //--exchange

                nbhExIns = exchangeNBH(inst, solution.second, solution.first);
             
                switch (arg2int[argv[3]]){
                    case 31: //--first
                        
                        improvementExIns = firstImprov_EI(nbhExIns, WT);
                        
                    break;

                    case 32: //--best
                        improvementExIns = bestImprov_EI(nbhExIns, WT);
                    break;
                    
                    default:
                        cout << "Invalid argument for initial solution, choose between --first and --best" << endl;
                    return 0;
                }

                if (improvementExIns == make_pair(0,0)){
                    improved = false;
                }else{
                    temp = solution.first[improvementExIns.first];
                    solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                    solution.first[improvementExIns.second] = temp;

                    WT = nbhExIns[improvementExIns].first;
                }

            break;

            case 23: //--insert
                nbhExIns = insertNBH(inst, solution.second, solution.first);

                switch (arg2int[argv[3]]){
                    case 31: //--first
                        improvementExIns = firstImprov_EI(nbhExIns, WT);
                    break;

                    case 32: //--best
                        improvementExIns = bestImprov_EI(nbhExIns, WT);
                    break;
                    
                    default:
                        cout << "Invalid argument for initial solution, choose between --first and --best" << endl;
                    return 0;
                }

                if (improvementExIns == make_pair(0,0)){
                    improved = false;
                }else{
                    temp = solution.first[improvementExIns.first];
                    solution.first[improvementExIns.first] = solution.first[improvementExIns.second];
                    solution.first[improvementExIns.second] = temp;

                    WT = nbhExIns[improvementExIns].first;
                }

            break;
            
            default:
                cout << "Invalid argument for neighborhood, choose between --transpose, --exchange and -- insert" << endl;
            return 0;
        }
    }
    
    auto end = chrono::high_resolution_clock::now();

    double elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    elapsed *= 1e-9;
    
    cout << "WT = " << WT << " --- Computation time = " << elapsed << endl;
    
    return 0;
}


// int main(int argc, char *argv[]){
    
//     /*Exercice 1.1 - part 1.*/

//     fstream fout;

//     fout.open("dat/Ex111_time.csv", ios::out | ios::trunc);
//     fout << "Instance,AlgCode,Time1,Time2,Time3,Time4,Time5\n";
//     fout.close();

//     fout.open("dat/Ex111_dev.csv", ios::out | ios::trunc);
//     fout << "Instance,AlgCode,Dev1,Dev2,Dev3,Dev4,Dev5\n";
//     fout.close();

//     string init[] = {"--randomInit", "--srz"};
//     string nbh[] = {"--transpose", "--exchange", "--insert"};
//     string pivot[] = {"--first", "--best"};

//     pair<long int, double> data;

//     for (auto const& file : directory_iterator("./Material/PFSP_instances")){

//         cout << "\n\e[1m---- Instance: " << file.path() << " ----\e[0m\n" << endl;

//         for (int a = 0; a < 2; a++){

//             cout << "\n\e[1m---- Init: " << init[a] << " ----\e[0m\n" << endl;

//             for (int b = 0; b < 3; b++){

//                 cout << "\n\e[1m---- Nbh: " << nbh[b] << " ----\e[0m\n" << endl;

//                 for (int c = 0; c < 2; c++){

//                     cout << "\n\e[1m---- Pivot: " << pivot[c] << " ----\e[0m\n" << endl;

//                     fout.open("dat/Ex111_time.csv", ios::out | ios::app);
//                     fout << file << "," << init[a] << nbh[b] << pivot[c];
//                     fout.close();

//                     fout.open("dat/Ex111_dev.csv", ios::out | ios::app);
//                     fout << file << "," << init[a] << nbh[b] << pivot[c];
//                     fout.close();

//                     for (int k = 0; k < 5; k++){

//                         cout << "\n\e[1m---- k = " << k << " ----\e[0m\n" << endl;

//                         data = exec(file.path().c_str(), init[a], nbh[b], pivot[c]);
                    
//                         fout.open("dat/Ex111_dev.csv", ios::out | ios::app);
//                         fout << "," << data.first;
//                         fout.close();

//                         fout.open("dat/Ex111_time.csv", ios::out | ios::app);
//                         fout << "," << data.second;
//                         fout.close();
//                     }

//                     fout.open("dat/Ex111_dev.csv", ios::out | ios::app);
//                     fout << "\n";
//                     fout.close();

//                     fout.open("dat/Ex111_time.csv", ios::out | ios::app);
//                     fout << "\n";
//                     fout.close();
//                 }
//             }
//         }
//     }

    
//     /********** - VND - **********/

//     fstream fout;
    
//     fout.open("dat/Ex12_time.csv", ios::out | ios::trunc);
//     fout << "Instance,AlgCode,Time\n";
//     fout.close();
    
//     fout.open("dat/Ex12_dev.csv", ios::out | ios::trunc);
//     fout << "Instance,AlgCode,Val\n";
//     fout.close();

//     string alg[] = {"RandInit - Transp -> Ex -> Ins", "RandInit - Transp -> Ins -> Ex", "SRZ - Transp -> Ex -> Ins", "SRZ - Transp -> Ins -> Ex"};

//     pair<long int, double> data;

//     for (auto const& file : directory_iterator("./Material/PFSP_instances")){
//         cout << "\n\e[1m---- Instance: " << file.path() << " ----\e[0m\n" << endl;

//         for (int a = 0; a < 4; a++){
//             cout << "\n\e[1m---- Algorithm: " << alg[a] << " ----\e[0m\n" << endl;

//             switch (a){
//             case 0:
//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close();

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close(); 

//                 data = VND_Rand_TEI(file.path().c_str());

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << "," << data.first << "\n";
//                 fout.close();

//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << "," << data.second << "\n";
//                 fout.close();
//             break;

//             case 1:
//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close();

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close(); 

//                 data = VND_Rand_TIE(file.path().c_str());

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << "," << data.first << "\n";
//                 fout.close();

//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << "," << data.second << "\n";
//                 fout.close();
//             break;

//             case 2:
//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close();

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close(); 

//                 data = VND_SRZ_TEI(file.path().c_str());

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << "," << data.first << "\n";
//                 fout.close();

//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << "," << data.second << "\n";
//                 fout.close();
//             break;

//             case 3:
//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close();

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << file << "," << alg[a];
//                 fout.close(); 

//                 data = VND_SRZ_TIE(file.path().c_str());

//                 fout.open("dat/Ex12_dev.csv", ios::out | ios::app);
//                 fout << "," << data.first << "\n";
//                 fout.close();

//                 fout.open("dat/Ex12_time.csv", ios::out | ios::app);
//                 fout << "," << data.second << "\n";
//                 fout.close();
//             break;
            
//             default:
//                 break;
//             }
//         }
//     }





//     //pair<long int, double> x = VND_SRZ_TEI("/home/breizhen/Nextcloud/Fac/M1/S8/INFO-H413 - Heuristic Optimization/permutation-flowshop-scheduling-problem/Material/PFSP_instances/DD_Ta051.txt");




//     return 0;
// }

