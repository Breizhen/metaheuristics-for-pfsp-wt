#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <numeric>
#include <iostream>
#include <vector>
#include <string>
#include <bits/stdc++.h>
#include <fstream>
#include <filesystem>
#include <chrono>

#include "pfspinstance.h"
#include "flowshop.h"

using namespace std;
using directory_iterator = std::filesystem::directory_iterator;

std::ostream& bold_on(std::ostream& os){ return os << "\e[1m"; }

std::ostream& bold_off(std::ostream& os){ return os << "\e[0m"; }


double randDouble(){ 
    return (double)(rand()) / (double)(RAND_MAX);
}

int randInt(int min, int max){
    return rand() % (max-min+1) + min;
}

void print_vect_int(vector<int> vec){
    cout << "[" << vec[0];
    for (int i = 1; i < vec.size(); i++){
        cout << ", " << vec[i];
    }
    cout << "]" << endl;
    
}

void print_vect_wspt(vector<pair<double,int>> vec){
    cout << "[" << vec[0].first;
    for (int i = 1; i < vec.size(); i++){
        cout << ", [" << vec[i].first << ", " << vec[i].second << "]";
    }
    cout << "]" << endl;
    
}

bool sortFunction(pair<double,int> x, pair<double,int> y){
    return x.first < y.first;
}

vector<pair<double,int>> WSPT(PfspInstance instance){
    vector<pair<double,int>> wspt;
    wspt.resize(instance.getNbJob()+1);
    wspt[0] = make_pair(DBL_MAX, 0);
    for (int i = 1; i <= instance.getNbJob(); i++){
        double acc = 0.;
        for (int j = 1; j <= instance.getNbMac(); j++){
            acc += (double)instance.getTime(i,j);
        }
        wspt[i] = make_pair(acc / instance.getPriority(i), i);
    }
    
    sort(wspt.begin()+1, wspt.end(), sortFunction);

    return wspt;
}


int binarySearch(vector<pair<double,int>> tab, int start, int end, double val){
    if (end - start <= 1){
        return start;
    }else{
        int mid = (end + start)/2;
        if (val == tab[mid].first){
            return mid;
        }else if (val < tab[mid].first){
            return binarySearch(tab, start, mid, val);
        }else{
            return binarySearch(tab, mid, end, val);
        }
    }
}


long int local_search(pair<vector<int>, vector<vector<int>>> sol, PfspInstance inst){
    bool improved = true;
    int temp;
    long int WT = getWT(sol.first, sol.second, inst);

    map<pair<int, int>, pair<long int, vector<vector<int>>>> nbh;
    pair<int, int> improvement;

    while (improved) {
        nbh = exchangeNBH(inst, sol.second, sol.first);
        improvement = bestImprov_EI(nbh, WT);

        if (improvement == make_pair(0,0)){
            improved = false;
        }else{
            temp = sol.first[improvement.first];
            sol.first[improvement.first] = sol.first[improvement.second];
            sol.first[improvement.second] = temp;
            WT = nbh[improvement].first;
        }
    }
    
    return WT;
}


//---------------------- SIMULATED ANNEALING ------------------------//



long int simulated_annealing(PfspInstance inst, long int t0, double alpha, int step, double time_out){
    auto start = chrono::high_resolution_clock::now();

    pair<vector<int>, vector<vector<int>>> currentSol = RZHeuristic(inst);
    long int currentZ = getWT(currentSol.first, currentSol.second, inst);

    pair<vector<int>, vector<vector<int>>> bestSol = currentSol;
    long int bestZ = currentZ;

    pair<int, int> permut;
    vector<int> newVec;
    vector<vector<int>> newEndTime;
    //pair<vector<int>, vector<vector<int>>> newSol;
    long int newZ;
    int temp;

    map<pair<int, int>, pair<long int, vector<vector<int>>>> nbh;
    map<pair<int, int>, pair<long int, vector<vector<int>>>>::iterator it; //iterator for nbh
    double temperature = (double)t0;
    long int delta;
    int k = 0; // iteraitions counter
    double p; // random number to know if we keep our non-improving solution 


    auto now = chrono::high_resolution_clock::now();
    double elapsed = chrono::duration_cast<chrono::nanoseconds>(now - start).count();
    elapsed *= 1e-9;

    while (elapsed < time_out){

        nbh = exchangeNBH(inst, currentSol.second, currentSol.first); //DEBUG
        it = nbh.begin();
        advance(it, rand() % nbh.size());
        
        permut = it->first;
        newZ = (it->second).first; //DEBUG
        newEndTime = (it->second).second;
        newVec = currentSol.first;
        temp = newVec[permut.first];
        newVec[permut.first] = newVec[permut.second];
        newVec[permut.second] = temp;

        if (newZ < currentZ){
            currentZ = newZ; //DEBUG
            currentSol = make_pair(newVec, newEndTime);
            if (newZ < bestZ){
                bestZ = newZ; //DEBUG
                bestSol = currentSol;
            }
        }else{
            p = randDouble();
            delta = newZ - currentZ;
            if (p < (double)exp(-1*delta/temperature)){
                currentZ = newZ; //DEBUG
                currentSol = make_pair(newVec, newEndTime);
            }
        }

        k++;
        temperature = (double)(pow(alpha, k/step) * t0);

        now = chrono::high_resolution_clock::now();
        elapsed = chrono::duration_cast<chrono::nanoseconds>(now - start).count();
        elapsed *= 1e-9;

    }
    
    return bestZ;
}



//----------------------- REACTIVE GRASP -----------------------//


pair<vector<int>, vector<vector<int>>> constrGRASP(PfspInstance inst, double alpha){
    vector<int> finSol;
    vector<pair<double,int>> wspt = WSPT(inst); //(wspt, job number)

    //print_vect_wspt(wspt);


    double lim;
    int rcl, index;
    pair<int,int> pickedInd; //(index in wspt, job number)
    while (wspt.size() > 1){

        lim = wspt[1].first + alpha*(wspt[wspt.size()-1].first - wspt[1].first);
        rcl = binarySearch(wspt, 1, wspt.size()-1, lim);
        index = randInt(1, rcl);
        pickedInd = make_pair(index, wspt[index].second);

        int size = finSol.size();
        if (size == 0){
            finSol.push_back(pickedInd.second);

        }else{
            vector<int> partSol = finSol;
            int *times = new int [size+1];

            for (int j = 0; j <= size; j++) {
                partSol.insert(partSol.begin() + j, pickedInd.second);
                times[j] = partialWCT(partSol, inst).first;
                partSol.erase(partSol.begin() + j);
            }
            finSol.insert(finSol.begin() + (min_element(times, times+size) - times), pickedInd.second);
            delete[] times;
        }

        wspt.erase(wspt.begin()+pickedInd.first);
    }

    vector<vector<int> > endTimes = partialWCT(finSol,inst).second;
    finSol.insert(finSol.begin(), 0); //We ignore first element (index 0)
    return make_pair(finSol, endTimes);
}


int pickAlpha(vector<double> accProb){
    double p = randDouble();

    int pickedInd = accProb.size() - 1;

    while (pickedInd > 0 && p > accProb[pickedInd - 1]){
        pickedInd--;
    }

    return pickedInd;    
}

void updateProba(vector<double>& probA, int zBest, int zWorst, vector<pair<double, int>> zAvg){
    int a_size = probA.size();
    vector<double> q(a_size);
    double sum_q = 0;
    for (int i = 0; i < a_size; i++){
        q[i] = (zWorst - zAvg[i].first)/(double)(zWorst - zBest);
        sum_q += q[i];
    }

    for (int i = 0; i < a_size; i++){
        probA[i] = q[i]/sum_q;
    }
}

void updateMean(pair<double, int> & mean, long int newElem){
    mean.second++;
    mean.first += (newElem - mean.first)/mean.second;
}


long int reactiveGRASP(PfspInstance inst, vector<double> alphas, double time_out, int updateA){
    auto start = chrono::high_resolution_clock::now();

    int a_size = alphas.size();
    vector<double> probA(a_size, ((double)1/(double)a_size)); // probabilities of the different alphas (initialized at same probability)
    vector<double> accProb = probA;
    /* accumulated probabilities to choose the right alpha for a given random double */
    for (int i = 1; i < a_size; i++){
        accProb[i] += accProb[i-1];
    }
    
    long int zBest = LONG_MAX;
    long int zWorst = 0;
    vector<pair<double, int>> zAvg(a_size, make_pair((double)0, 0));

    int counter = 0;
    int currentAlpha;
    pair<vector<int>, vector<vector<int>>> constrSol;
    long int zCurrent;
    
    auto now = chrono::high_resolution_clock::now();
    double elapsed = chrono::duration_cast<chrono::nanoseconds>(now - start).count();
    elapsed *= 1e-9;
    while (elapsed < time_out){
        currentAlpha = pickAlpha(probA);

        constrSol = constrGRASP(inst, alphas[currentAlpha]);

        zCurrent = local_search(constrSol, inst);

        if (zCurrent < zBest){
            zBest = zCurrent;
        }
        if (zCurrent > zWorst){
            zWorst = zCurrent;
        }
        updateMean(zAvg[currentAlpha], zCurrent);

        counter++;
        if (counter % updateA == 0){
            updateProba(probA, zBest, zWorst, zAvg);
            accProb[0] = probA[0];
            for (int i = 1; i < a_size; i++){
                accProb[i] = accProb[i-1] + probA[i];
            }
        }
        
        now = chrono::high_resolution_clock::now();
        elapsed = chrono::duration_cast<chrono::nanoseconds>(now - start).count();
        elapsed *= 1e-9;
    }
    
    return zBest;
}



/*------------- Benchmarks -------------*/

void fine_tunning_SA(){
    fstream fout;

    fout.open("dat/SA_tuning50_1.csv", ios::out | ios::trunc);
    fout << "alpha,T0,1_1,1_2,1_3,2_1,2_2,2_3,3_1,3_2,3_3,4_1,4_2,4_3,5_1,5_2,5_3\n";
    fout.close();

    fout.open("dat/SA_tuning100_1.csv", ios::out | ios::trunc);
    fout << "alpha,T0,1_1,1_2,1_3,2_1,2_2,2_3,3_1,3_2,3_3,4_1,4_2,4_3,5_1,5_2,5_3\n";
    fout.close();

    vector<double> alphas{0.85, 0.88, 0.92, 0.95, 0.99};
    vector<long int> init_temp{1000, 2000, 5000, 8000, 10000};
    const int L_50 = 50*51;
    const int L_100 = 100*101;
    const double time_out_50 = 11.0;
    const double time_out_100 = 130.0;
    PfspInstance inst;
    string path;
    long int sol;

    for (double alpha : alphas){
        cout << "--- alpha = " << alpha << " ---" << endl;

        for (long int t0 : init_temp){
            cout << "--- T_0 = " << t0 << " ---" << endl;

            fout.open("dat/SA_tuning50_1.csv", ios::out | ios::app);
            fout << alpha << "," << t0 << ",";
            fout.close();

            fout.open("dat/SA_tuning100_1.csv", ios::out | ios::app);
            fout << alpha << "," << t0;
            fout.close();

            for (int i = 1; i < 6; i++){
                path = "Material/PFSP_instances/DD_Ta05" + to_string(i) + ".txt";
                inst.readDataFromFile(path.c_str());

                for (int rep = 0; rep < 3; rep++){
                    sol = simulated_annealing(inst, t0, alpha, L_50, time_out_50);
                    fout.open("dat/SA_tuning50_1.csv", ios::out | ios::app);
                    fout << "," << sol;
                    fout.close();
                }

                path = "Material/PFSP_instances/DD_Ta08" + to_string(i) + ".txt";
                inst.readDataFromFile(path.c_str());

                for (int rep = 0; rep < 3; rep++){
                    sol = simulated_annealing(inst, t0, alpha, L_100, time_out_100);
                    fout.open("dat/SA_tuning100_1.csv", ios::out | ios::app);
                    fout << "," << sol;
                    fout.close();
                }
                
            }



            fout.open("dat/SA_tuning50_1.csv", ios::out | ios::app);
            fout << "\n";
            fout.close();

            fout.open("dat/SA_tuning100_1.csv", ios::out | ios::app);
            fout << "\n";
            fout.close();
        }
    }
}

void fine_tunning_SA_2(){
    fstream fout;

    fout.open("dat/SA_tuning50_2.csv", ios::out | ios::trunc);
    fout << "L,1_1,1_2,1_3,2_1,2_2,2_3,3_1,3_2,3_3,4_1,4_2,4_3,5_1,5_2,5_3\n";
    fout.close();

    fout.open("dat/SA_tuning100_2.csv", ios::out | ios::trunc);
    fout << "L,1_1,1_2,1_3,2_1,2_2,2_3,3_1,3_2,3_3,4_1,4_2,4_3,5_1,5_2,5_3\n";
    fout.close();

    vector<long int> L50{957, 1275, 1594, 1913, 2232, 2550, 2869, 3188, 3507, 3825};
    vector<long int> L100{7575, 8137, 8698, 9259, 9820, 10381, 10942, 11503, 12064, 12625};
    const double alpha_50 = 0.99;
    const long int t0_50 = 2000;
    const double alpha_100 = 0.85;
    const long int t0_100 = 1000;
    const double time_out_50 = 11.0;
    const double time_out_100 = 130.0;
    PfspInstance inst;
    string path;
    long int sol;

    for (int it = 0; it < 10; it++){
        fout.open("dat/SA_tuning50_2.csv", ios::out | ios::app);
        fout << L50[it];
        fout.close();

        fout.open("dat/SA_tuning100_2.csv", ios::out | ios::app);
        fout << L100[it];
        fout.close();

        for (int i = 1; i < 6; i++){
            path = "Material/PFSP_instances/DD_Ta05" + to_string(i) + ".txt";
            inst.readDataFromFile(path.c_str());

            for (int rep = 0; rep < 3; rep++){
                sol = simulated_annealing(inst, t0_50, alpha_50, L50[it], time_out_50);
                fout.open("dat/SA_tuning50_2.csv", ios::out | ios::app);
                fout << "," << sol;
                fout.close();
            }

            path = "Material/PFSP_instances/DD_Ta08" + to_string(i) + ".txt";
            inst.readDataFromFile(path.c_str());

            for (int rep = 0; rep < 3; rep++){
                sol = simulated_annealing(inst, t0_100, alpha_100, L100[it], time_out_100);
                fout.open("dat/SA_tuning100_2.csv", ios::out | ios::app);
                fout << "," << sol;
                fout.close();
            }
            

        }

        fout.open("dat/SA_tuning50_2.csv", ios::out | ios::app);
        fout << "\n";
        fout.close();

        fout.open("dat/SA_tuning100_2.csv", ios::out | ios::app);
        fout << "\n";
        fout.close();

    }
}

void fine_tunning_SA_3(){
    fstream fout;

    fout.open("dat/SA_tuning50_3.csv", ios::out | ios::trunc);
    fout << "L,1_1,1_2,1_3,2_1,2_2,2_3,3_1,3_2,3_3,4_1,4_2,4_3,5_1,5_2,5_3\n";
    fout.close();

    vector<long int> L50{1000, 1027, 1053, 1079, 1106, 1132, 1158, 1185, 1211, 1237, 1264, 1290, 1316, 1343, 1369, 1395, 1422, 1448, 1474, 1500};
    const double alpha_50 = 0.99;
    const long int t0_50 = 2000;
    const double time_out_50 = 30.0;
    PfspInstance inst;
    string path;
    long int sol;

    for (int it = 0; it < 20; it++){
        fout.open("dat/SA_tuning50_3.csv", ios::out | ios::app);
        fout << L50[it];
        fout.close();

        for (int i = 1; i < 6; i++){
            path = "Material/PFSP_instances/DD_Ta05" + to_string(i) + ".txt";
            inst.readDataFromFile(path.c_str());

            for (int rep = 0; rep < 3; rep++){
                sol = simulated_annealing(inst, t0_50, alpha_50, L50[it], time_out_50);
                fout.open("dat/SA_tuning50_3.csv", ios::out | ios::app);
                fout << "," << sol;
                fout.close();
            }            

        }

        fout.open("dat/SA_tuning50_3.csv", ios::out | ios::app);
        fout << "\n";
        fout.close();

    }
}

void benchmark_SA_50(){
    fstream fout;

    //fout.open("dat/SA_bench50.csv", ios::out | ios::trunc);
    //fout << "inst,rep_1,rep_2,rep_3,rep_4,rep_5\n";
    //fout.close();

    const double alpha = 0.99;
    const long int T0 = 2000;
    const long int L = 1422;
    const double time_out = 107.0;
    PfspInstance inst;
    string path;
    long int sol;

    for (int it = 54; it < 61; it++){
        cout << "--- inst: DD_Ta0" << it << " ---" << endl;

        fout.open("dat/SA_bench50.csv", ios::out | ios::app);
        fout << "DD_Ta0" << it;
        fout.close();

        path = "Material/PFSP_instances/DD_Ta0" + to_string(it) + ".txt";
        inst.readDataFromFile(path.c_str());

        for (int rep = 0; rep < 5; rep++){
            cout << "--- rep: " << rep << " ---" << endl;

            sol = simulated_annealing(inst, T0, alpha, L, time_out);
            fout.open("dat/SA_bench50.csv", ios::out | ios::app);
            fout << "," << sol;
            fout.close();
        }    

        fout.open("dat/SA_bench50.csv", ios::out | ios::app);
        fout << "\n";
        fout.close();
    }
    
}

void benchmark_SA_100(){
    fstream fout;

    fout.open("dat/SA_bench100.csv", ios::out | ios::trunc);
    fout << "inst,rep_1,rep_2,rep_3,rep_4,rep_5\n";
    fout.close();

    const double alpha = 0.85;
    const long int T0 = 1000;
    const long int L = 9820;
    const double time_out = 1300.0;
    PfspInstance inst;
    string path;
    long int sol;

    for (int it = 81; it < 91; it++){
        cout << "--- inst: DD_Ta0" << it << " ---" << endl;

        fout.open("dat/SA_bench100.csv", ios::out | ios::app);
        fout << "DD_Ta0" << it;
        fout.close();

        path = "Material/PFSP_instances/DD_Ta0" + to_string(it) + ".txt";
        inst.readDataFromFile(path.c_str());

        for (int rep = 0; rep < 5; rep++){
            cout << "--- rep: " << rep << " ---" << endl;

            sol = simulated_annealing(inst, T0, alpha, L, time_out);
            fout.open("dat/SA_bench100.csv", ios::out | ios::app);
            fout << "," << sol;
            fout.close();
        }    

        fout.open("dat/SA_bench100.csv", ios::out | ios::app);
        fout << "\n";
        fout.close();
    }
    
}



void benchmark_RG_50(){
    fstream fout;

    fout.open("dat/RG_bench50.csv", ios::out | ios::trunc);
    fout << "inst,rep_1,rep_2,rep_3,rep_4,rep_5\n";
    fout.close();

    const vector<double> alphas{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8};
    const int NAlpha = 32;
    const double time_out = 107.0;
    PfspInstance inst;
    string path;
    long int sol;

    for (int it = 51; it < 61; it++){
        cout << "--- inst: DD_Ta0" << it << " ---" << endl;

        fout.open("dat/RG_bench50.csv", ios::out | ios::app);
        fout << "DD_Ta0" << it;
        fout.close();

        path = "Material/PFSP_instances/DD_Ta0" + to_string(it) + ".txt";
        inst.readDataFromFile(path.c_str());

        for (int rep = 0; rep < 5; rep++){
            cout << "--- rep: " << rep << " ---" << endl;

            sol = reactiveGRASP(inst, alphas, time_out, NAlpha);
            fout.open("dat/RG_bench50.csv", ios::out | ios::app);
            fout << "," << sol;
            fout.close();
        }    

        fout.open("dat/RG_bench50.csv", ios::out | ios::app);
        fout << "\n";
        fout.close();
    }
    
}

void benchmark_RG_100(){
    fstream fout;

    fout.open("dat/RG_bench100.csv", ios::out | ios::trunc);
    fout << "inst,rep_1,rep_2,rep_3,rep_4,rep_5\n";
    fout.close();

    const vector<double> alphas{0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8};
    const int NAlpha = 32;
    const double time_out = 1300.0;
    PfspInstance inst;
    string path;
    long int sol;

    for (int it = 81; it < 91; it++){
        cout << "--- inst: DD_Ta0" << it << " ---" << endl;

        fout.open("dat/RG_bench100.csv", ios::out | ios::app);
        fout << "DD_Ta0" << it;
        fout.close();

        path = "Material/PFSP_instances/DD_Ta0" + to_string(it) + ".txt";
        inst.readDataFromFile(path.c_str());

        for (int rep = 0; rep < 5; rep++){
            cout << "--- rep: " << rep << " ---" << endl;

            sol = reactiveGRASP(inst, alphas, time_out, NAlpha);
            fout.open("dat/RG_bench100.csv", ios::out | ios::app);
            fout << "," << sol;
            fout.close();
        }    

        fout.open("dat/RG_bench100.csv", ios::out | ios::app);
        fout << "\n";
        fout.close();
    }
    
}

int main(int argc, char const *argv[]){

    if (argc < 3){
        cout << "ERROR: Usage: ./flowshopMH <MetaH> <pathToInstance>" << endl;
        cout << "With <MetaH> = --SA  or  --RG";
        return 0;
    }

    PfspInstance inst;
    if (!inst.readDataFromFile(argv[2])) return 0;

    long int sol;

    if ((string)argv[1] == "--SA"){
        long int t0;
        double alpha, time_out;
        int step;

        cout << "Enter an initial temperature for the algorithm (in the form of an integer): " << endl;
        cin >> t0;

        cout << "\nEnter a cooling coefficient for the algorithm (in the form of a floating point number): " << endl;
        cin >> alpha;

        cout << "\nEnter a plateau length for the algorithm (in the form of an integer): " << endl;
        cin >> step;

        cout << "\nEnter a time limit for the algorithm (in seconds, in the form of a floating point number): " << endl;
        cin >> time_out;

        sol = simulated_annealing(inst, t0, alpha, step, time_out);

        cout << "\n\n" << bold_on << "With the parameters entered, the returned solution is: " << bold_off << sol << endl;

    }else if ((string)argv[1] == "--RG"){
        vector<double> alphas;
        int nbAlphas, updateA;
        double a, time_out;

        cout << "Enter the number of 'alpha parameters'  you want to use in the algorithm (in the form of an integer): ";
        cin >> nbAlphas;
        alphas.reserve(nbAlphas);

        for (int i = 0; i < nbAlphas; i++){
            cout << "\n\nEnter an alpha parameter (in the form of a floating point number): ";
            cin >> a;
            alphas.push_back(a);
        }
        cout << "Alpha list filled!" << endl;

        cout << "\nEnter the update parameter (in the form of an integer): " << endl;
        cin >> updateA;

        cout << "\nEnter a time limit for the algorithm (in seconds, in the form of a floating point number): " << endl;
        cin >> time_out;

        sol = reactiveGRASP(inst, alphas, time_out, updateA);

        cout << "\n\n" << bold_on << "With the parameters entered, the returned solution is: " << bold_off << sol << endl;        
    }else{
        cout << "Invalid argument for the choice of the metaheuristic, choose between --SA and --RG" << endl;
        return 0;
    }
    

    return 0;
}

